# !/user/bin/env python
# -*- coding : utf-8 -*-

# 1 About the Data: the feature map extracted by maskrcnn
import pickle

train_feature_file = "./Results/coco_2014_train_feature/coco_2014_train.txt"
train_label_file = "./Results/coco_2014_train_feature/coco_2014_train_labels.txt"
val_feature_file = "./Results/coco_2014_valminusminival_feature/coco_2014_valminusminival.txt"
val_label_file = "./Results/coco_2014_valminusminival_feature/coco_2014_valminusminival_labels.txt"
test_feature_file = "./Results/coco_2014_minival_feature/coco_2014_minival.txt"
test_label_file = "./Results/coco_2014_minival_feature/coco_2014_minival_labels.txt"

with open(train_feature_file, "rb") as fea1:
    train_features = pickle.load(fea1)
with open(train_label_file, "rb") as lab1:
    train_labels = pickle.load(lab1)
with open(val_feature_file, "rb") as fea2:
    val_features = pickle.load(fea2)
with open(val_label_file, "rb") as lab2:
    val_labels = pickle.load(lab2)
with open(test_feature_file, "rb") as fea3:
    test_features = pickle.load(fea3)
with open(test_label_file, "rb") as lab3:
    test_labels = pickle.load(lab3)

from torch.utils import data

class myData(data.Dataset):
    def __init__(self, aim_features, aim_labels):
        ##Arguments:
        # aim_features, aim_labels were extracted from maskrcnn-benchmark by lb
        # aim_features: list[ list[Tensor(256,7,7)] ]
        # aim_labels  : list[ list[tensor(), tensor()] ]  鈥斺€斺€斺€攖ransition form
        super(myData, self).__init__()
        features = []
        labels = []
        for idx in range(len(aim_labels)):
            aim_labels_idx = torch.cat(tuple(aim_labels[idx]), 0)

            features.extend(aim_features[idx])
            labels.extend(aim_labels_idx)

        labels = [label-1 for label in labels]  #turn label [1,80] to [0,79]
        self.features = features
        self.labels = labels

    def __getitem__(self, index):
        return self.features[index], self.labels[index]

    def __len__(self):
        return len(self.labels)

# 2 Define a new ConvNet
import torch
import torch.nn as nn
import torch.nn.functional as F

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(256, 384, 3, padding=2) #9*9*384
        self.bn1 = nn.BatchNorm2d(384)

        self.conv2 = nn.Conv2d(384, 384, 3, padding=1) #9*9*384
        #self.bn2 = nn.BatchNorm2d(384) #same with bn1
        self.conv3 = nn.Conv2d(384, 384, 3, padding=1) #9*9*384
        #self.bn3 = nn.BatchNorm2d(384) #same with bn1
        self.conv4 = nn.Conv2d(384, 256, 3, padding=1) #9*9*256
        self.pool1 = nn.MaxPool2d(3, 2, padding=1)

        self.fc1 = nn.Linear(5*5*256, 4096)
        self.fc2 = nn.Linear(4096, 4096)
        self.fc3 = nn.Linear(4096, 80)

    def forward(self, x):
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn1(self.conv2(x)))
        x = F.relu(self.bn1(self.conv3(x)))
        x = F.relu(self.conv4(x))
        x = self.pool1(x)

        x = x.view(-1, 5*5*256)
        x = F.relu(self.fc1(x))  #add dropout
        x = F.dropout(x, training=self.training)
        x = F.relu(self.fc2(x))
        x = F.dropout(x, training=self.training)
        x = self.fc3(x)

        return x

import copy
import torch.optim as optim
import matplotlib.pyplot as plt

def train_val(dataloaders, epoch_num):

    device = torch.device("cuda:1" if torch.cuda.is_available() else "cpu")
    net = Net().to(device)

    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

    train_loss_list = []
    train_acc_list = []
    train_top5_list = []
    val_loss_list = []
    val_acc_list = []
    val_top5_list = []

    best_model_wts = copy.deepcopy(net.state_dict())
    best_acc = 0.0

    for epoch in range(epoch_num):

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                net.train()  # Set model to training mode
            else:
                net.eval()  # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0
            running_top5 = 0

            # Iterate over data.
            for _, (inputs, labels) in enumerate(dataloaders[phase], 0):
                inputs = inputs.to(device)
                labels = labels.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                outputs = net(inputs)
                loss = criterion(outputs, labels)

                _, top1 = torch.max(outputs.data, 1)
                _, top5 = torch.topk(outputs.data, 5, 1)

                if phase == 'train':
                    loss.backward()
                    optimizer.step()

                running_loss += loss.item() * inputs.size(0)
                running_corrects += (top1 == labels).sum().item()
                for bs in range(labels.size(0)):
                    if labels[bs] in top5[bs]:
                        running_top5 += 1

            epoch_loss = running_loss / len(dataloaders[phase].dataset)
            epoch_acc = running_corrects / len(dataloaders[phase].dataset)
            epoch_top5 = running_top5 / len(dataloaders[phase].dataset)

            # deep copy the model
            if phase == 'val' and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model_wts = copy.deepcopy(net.state_dict())
            if phase == 'val':
                val_loss_list.append(epoch_loss)  # Why not .extend
                val_acc_list.append(epoch_acc)
                val_top5_list.append(epoch_top5)
            if phase == 'train':
                train_loss_list.append(epoch_loss)
                train_acc_list.append(epoch_acc)
                train_top5_list.append(epoch_top5)

        print()

    net.load_state_dict(best_model_wts)
    torch.save(net.state_dict(), "model_train_epoch{}_best.pth".format(epoch_num))

    ''' Following is for plot'''
    x = range(epoch_num)

    plt.figure()  # loss img
    plt.plot(x, train_loss_list, '.-', color="blue", label='training loss')
    plt.plot(x, val_loss_list, '.-', color="red", label="validation loss")
    plt.legend(loc='best')
    plt.savefig('loss.png')

    plt.clf()  # acc img
    plt.plot(x, train_acc_list, '.-', color="blue", label='training accuracy')
    plt.plot(x, val_acc_list, '.-', color="red", label='validation accuracy')
    plt.legend(loc='best')
    plt.savefig('acc.png')

    plt.clf()  # top5 right img
    plt.plot(x, train_top5_list, '.-', color="blue", label='training top5-acc')
    plt.plot(x, val_top5_list, '.-', color="red", label='validation top5-acc')
    plt.legend(loc='best')
    plt.savefig('top5-acc.png')
    
    print('Finished!')


if __name__ == '__main__':
    import torch
    from torch.utils import data
    import random
    import time
    random.seed(111)

    start = time.time()

    trainset_all = myData(train_features, train_labels)
    train_len = len(trainset_all)
    
    random_train_idx = random.sample(range(train_len), int(train_len/10))
    trainset = torch.utils.data.Subset(trainset_all, random_train_idx)
    trainloader = data.DataLoader(trainset, batch_size=2, shuffle=True, num_workers=0)

    testset = myData(test_features, test_labels)
    test_len = len(testset)
    valset_all = myData(val_features, val_labels)
    val_len = len(valset_all)

    random_val_idx = random.sample(range(val_len), test_len)
    valset = torch.utils.data.Subset(valset_all, random_val_idx)
    valloader = data.DataLoader(valset, batch_size=2, shuffle=True, num_workers=0)

    dataloaders = {'train':trainloader,'val':valloader}

    epoch_num = 150
    train_val(dataloaders, epoch_num)

    end = time.time()
    print("Total time : {} s".format(end-start))










